﻿Module Module1
    Sub Main()
        ' Two Dimensional Array
        Dim array2D As Integer(,) = New Integer(2, 1) {{4, 5}, {5, 0}, {3, 1}}

        Console.WriteLine("---Mang hai chieu---")
        For i As Integer = 0 To 3 - 1
            For j As Integer = 0 To 2 - 1
                Console.WriteLine("a[{0},{1}] = {2}", i, j, array2D(i, j))
            Next
        Next

        Console.WriteLine("Press Enter Key to Exit..")
        Console.ReadLine()
    End Sub
End Module

